﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace StackAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<string> stack_element = new Stack<string>();
            Console.WriteLine("Enter first string1: ");
            String s1 = Console.ReadLine();
            Console.WriteLine("Enter second string2: ");
            String s2 = Console.ReadLine();
            Console.WriteLine("Enter fourth string4: ");
            String s4 = Console.ReadLine();
            Console.WriteLine("Enter fifth string5: ");
            String s5 = Console.ReadLine();

            stack_element.Push(s1);
            stack_element.Push(s2);
            stack_element.Push(s4);
            stack_element.Push(s5);

            Console.WriteLine("Elements in the stack: ");
            foreach(String value in stack_element)
            {
                Console.WriteLine(value);
            }

            //Popping of stack object
            String st5 = stack_element.Pop();
            String st4 = stack_element.Pop();
            Console.WriteLine("Elements left in the stack: ");
            foreach (String value in stack_element)
            {
                Console.WriteLine(value);
            }

            //Addition of new object in the stack
            Console.WriteLine("Enter third string3: ");
            String s3 = Console.ReadLine();
            stack_element.Push(s3);
            stack_element.Push(st4);
            stack_element.Push(st5);

            Console.WriteLine("Elements in the final stack: ");
            foreach (String value in stack_element)
            {
                Console.WriteLine(value);
            }

            Console.WriteLine("This is for Git");//changed code for git

            //Console.WriteLine("Top element of the stack: "+stack_element.Peek());
            Console.ReadLine();

        }
    }
}
